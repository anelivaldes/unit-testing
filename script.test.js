const aux = require('./script');
const axios = require('axios');

it('calculate factorial of 5', () => {
    let v = aux.factorial(4);
    expect(v).toEqual(24);
});

// An async issue: the test exit before the promise gets resolved or rejected

it('get people', () => {
    expect.assertions(1);// Use this to check if your async test is really working
    let v = aux.getPeople(axios);
    v.then((resp) => {
        expect(resp.name).toEqual('Luke Skywalker');
    })
});

// To approach to test async functions: always return the promise or use done

fit('get people', (done) => {
    aux.getPeople(axios).then((resp) => {
        expect(resp.name).toEqual('Luke Skywalker');
        done();
    })
});

it('get people2', () => {
    return aux.getPeople(axios).then((resp) => {
        expect(resp.name).toEqual('Luke Skywalker');
    })
});

// Using Mocks to replace real api requests, for performance and isolation
// We are using axios, but we do not need to test axios, is already tested
// So instead of axios we can use a mock function, but our getPeople use axios
// so we need to extract axios and injected as a dependency of our function
// In that way it will be easy to test.
//

it('get people successfully', () => {
    let successResponse = {
        data: {
            name: 'John Smith'
        }
    };
    let axiosMock = {
        get: jest.fn().mockReturnValue(Promise.resolve(successResponse))
    };
    return aux.getPeople(axiosMock).then((resp) => {
        expect(resp.name).toEqual('Luke Skywalker');
    })
});

// Make a promise rejection, important to note this:
// If the error is not rethrow in the catch block or a or return Promise.reject(error), then
// is the catch block is present the promise will be consider resolved here

fit('get error message on fail', () => {
    let failResponse = 'An error happened!';
    let axiosMock = {
        get: jest.fn().mockReturnValue(Promise.reject(failResponse))
    };
    expect.assertions(1);
    return aux.getPeople(axiosMock).catch((error) => {
        expect(error).toEqual('An error happened!');
    })
});
