//const axios = require('axios');

function factorial(n) {
    if (n === 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

function getPeople(axios) {
    return axios.get('https://swapi.co/api/people/1')
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            throw error;
            // or return Promise.reject(error)
        })
}

//getPeople().then((resp) => console.log(resp));

module.exports = {factorial, getPeople};

